export default function formattedDate (data, hours) {
  let x = new Date(data)
  x = (x.getDate() <= 9 ? '0' + x.getDate() : x.getDate()) + '/' + (x.getMonth() <= 9 ? '0' + (x.getMonth() + 1) : (x.getMonth() + 1)) + '/' + x.getFullYear() + (hours && (` ${x.getHours()}:${x.getMinutes()}`))
  return x
}
