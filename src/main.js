import { createSSRApp  } from 'vue'
import App from './App.vue'
import "./assets/style.scss"
import router from './router';
import clickOutside from './utils/click-outside';
import store from './vuex/store';
import { Quasar } from 'quasar'
// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'
// Import Quasar css
import 'quasar/src/css/index.sass'

// FIREBASE
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
const firebaseConfig = {
  apiKey: "AIzaSyAfcF7lLunO01_23AuiDzajXkBfZltdzdo",
  authDomain: "avtourist-region.firebaseapp.com",
  databaseURL: "https://avtourist-region-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "avtourist-region",
  storageBucket: "avtourist-region.appspot.com",
  messagingSenderId: "644039655474",
  appId: "1:644039655474:web:c1684f0de969d9145dbc2b",
  measurementId: "G-N6VGP0B02R"
};
const appFirebase = initializeApp(firebaseConfig);

store.state.user = localStorage.user && JSON.parse(localStorage.getItem('user'))

createSSRApp(App)
  .use(store)
  .use(Quasar, {
    plugins: {}, // import Quasar plugins and add here
  })
  .use(router)
  .directive('click-outside', clickOutside)
  .mount('#app')
