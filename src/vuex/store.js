import { createStore } from "vuex";

export default createStore({
  state: {
    user: {
      uid: '',
    },
    isAdmin: true
  },
  mutations: {
    SET_ADMIN(state) {
      state.isAdmin = !state.isAdmin
    },
    LOGOUT(state) {
      state.user = { uid: '' }
      localStorage.removeItem('user')
    }
  },
  actions: {
    setAdmin ({commit}) {
      commit('SET_ADMIN')
    },
    logout ({commit}) {
      commit('LOGOUT')
    }
  },
  getters: {
    isAdmin: state => state.isAdmin,
    isAuth: state => state.user?.uid && true || false
  }
});
// export const store = reactive({
//   user: {},
//   profile: {},
//   shop: [],
//   basket: []
// })
// export let basket = reactive([])
