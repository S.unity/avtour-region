import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

// const isServer = typeof window === 'undefined';
const history = createWebHistory();
const routes = [
    {
        path: '/',
        name: 'Index',
        component: ()=> import('../views/Index.vue'),
    },
    {
        path: '/auth',
        name: 'Auth',
        component: ()=> import('../views/Auth.vue'),
    },
    {
        path: '/page/narusheniya-i-zashchita/:id',
        name: 'Chance',
        props: true,
        component: ()=> import('../views/Chance.vue'),
    },
    {
        path: '/narusheniya-i-zashchita/:id',
        name: 'Alko',
        props: true,
        component: ()=> import('../views/Alko.vue'),
    },
    {
        path: '/wins',
        name: 'Wins',
        component: ()=> import('../views/Wins.vue'),
    },
    { path: '/:pathMatch(.*)*', component: ()=> import('../views/Page404.vue') }
];
const router = createRouter({
    history,
    routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;

    }

    if (to.hash) {
      return window.scrollTo({
        top: document.querySelector(to.hash).offsetTop,
        behavior: 'smooth'
      })
    }
    return { left: 0, top: 0 }
  }
});

export default router;
